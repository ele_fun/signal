package main

import (
	"log"
	"os"
	"os/signal"
	"time"
)

func main() {
	quit := make(chan os.Signal, 1)
	now := time.Now()

	signal.Notify(quit, os.Interrupt)

	for {
		select {
		case <-quit:
			log.Println("\nElapse time: ", time.Since(now).Seconds())
			os.Exit(0)

		default:
			log.Println("waiting")
			time.Sleep(1 * time.Second)
		}
	}
}
